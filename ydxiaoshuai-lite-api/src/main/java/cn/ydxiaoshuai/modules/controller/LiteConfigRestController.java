package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.config.LiteConfigCopywriteResult;
import cn.ydxiaoshuai.common.api.vo.config.LiteConfigSlideResult;
import cn.ydxiaoshuai.common.constant.ApiCodeConts;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.entity.LiteConfigCopywrite;
import cn.ydxiaoshuai.modules.entity.LiteConfigSlide;
import cn.ydxiaoshuai.modules.service.ILiteConfigCopywriteService;
import cn.ydxiaoshuai.modules.service.ILiteConfigSlideService;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * @author 小帅丶
 * @className LiteConfigRestController
 * @Description 小程序首页菜单、滚动公告、轮播图接口
 * @Date 2020/4/30-16:31
 **/
@Controller
@RequestMapping(value="/rest/lite/config")
@Scope("prototype")
@Api(tags = {"首页轮播、刷新文案API"})
@Slf4j
public class LiteConfigRestController extends ApiRestController {
    @Autowired
    private ILiteConfigSlideService liteConfigSlideService;
    @Autowired
    private ILiteConfigCopywriteService liteConfigCopywriteService;
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    /**
     * @Author 小帅丶
     * @Description 首页轮播图
     * @Date  2020/3/25 11:35
     * @return org.springframework.http.ResponseEntity<java.lang.Object>
     **/
    @ApiOperation(value = "首页轮播图", notes = "首页轮播图")
    @RequestMapping(value = "/index/slide",method = RequestMethod.GET)
    public ResponseEntity<Object> getIndexSlideList(){
        LiteConfigSlideResult bean = new LiteConfigSlideResult();
        try {
            startTime = System.currentTimeMillis();
            LambdaQueryWrapper<LiteConfigSlide> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(LiteConfigSlide::getIsShow, 0).orderByAsc(LiteConfigSlide::getSortId);
            List<LiteConfigSlide> list = liteConfigSlideService.list(queryWrapper);
            if(list.size()>0){
                bean = apiBeanUtil.dealIndexSlideListData(list);
            }else{
                bean.fail("暂无数据", ApiCodeConts.LIST_NULL);
            }
        } catch (Exception e){
            errorMsg = e.getMessage();
            bean.error(ApiCodeConts.MESSAGE_ERROR);
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis()-startTime);
        //响应的内容
        beanStr = JSON.toJSONString(bean);
        //放日志
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip,requestURI, errorMsg, LogTypeConts.INDEX_SLIDE,"system",userAgent);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

    /**
     * @Description 获取刷新内容接口
     * @Author 小帅丶
     * @Date  2020年3月3日09:55:56
     * @return org.springframework.http.ResponseEntity<java.lang.Object>
     **/
    @ApiOperation("获取刷新文案内容")
    @RequestMapping(value = "/v1/random_text", method = {RequestMethod.GET})
    public ResponseEntity<Object> getRandomText(){
        LiteConfigCopywriteResult bean = new LiteConfigCopywriteResult();
        try {
            startTime = System.currentTimeMillis();
            List<LiteConfigCopywrite> copywriteList = liteConfigCopywriteService.list();
            if(copywriteList.size()>0){
                bean = apiBeanUtil.dealIndexCopywriteListData(copywriteList);
            }else{
                bean.fail("暂无数据", ApiCodeConts.LIST_NULL);
            }
        }catch (Exception e){
            errorMsg = e.getMessage();
            bean.error(ApiCodeConts.MESSAGE_ERROR);
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis()-startTime);
        //响应的内容
        beanStr = JSON.toJSONString(bean);
        //放日志
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip,requestURI, errorMsg, LogTypeConts.INDEX_COPYWRITE,"system",userAgent);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

}
