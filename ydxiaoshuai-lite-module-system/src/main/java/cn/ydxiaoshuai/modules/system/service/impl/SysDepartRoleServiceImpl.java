package cn.ydxiaoshuai.modules.system.service.impl;

import cn.ydxiaoshuai.modules.system.entity.SysDepartRole;
import cn.ydxiaoshuai.modules.system.mapper.SysDepartRoleMapper;
import cn.ydxiaoshuai.modules.system.service.ISysDepartRoleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 部门角色
 * @Author: 小帅丶
 * @Date:   2020-02-12
 * @Version: V1.0
 */
@Service
public class SysDepartRoleServiceImpl extends ServiceImpl<SysDepartRoleMapper, SysDepartRole> implements ISysDepartRoleService {

}
