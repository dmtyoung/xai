package cn.ydxiaoshuai.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.ydxiaoshuai.modules.system.entity.SysPosition;

/**
 * @Description: 职务表
 * @Author: 小帅丶
 * @Date: 2019-09-19
 * @Version: V1.0
 */
public interface ISysPositionService extends IService<SysPosition> {

}
