package cn.ydxiaoshuai.modules.liteuser.service;

import cn.ydxiaoshuai.modules.liteuser.entity.LiteUserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信用户信息表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface ILiteUserInfoService extends IService<LiteUserInfo> {
    void updateUserDays();
}
