package cn.ydxiaoshuai.common.util.oss;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.region.Region;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 小帅丶
 * @className TencentOssUtil
 * @Description 腾讯云存储
 * @Date 2020/9/4-16:06
 **/
@Component
public class TencentOssUtil {
    /**
     * 腾讯OSS应用参数KEY
     **/
    @Value(value = "${oss.tencent.accessKey}")
    private String accessKey;
    /**
     * 腾讯OSS应用参数SECRET
     **/
    @Value(value = "${oss.tencent.secretKey}")
    private String secretKey;
    /**
     * 腾讯OSS应用参数bucket的区域
     **/
    @Value(value = "${oss.tencent.bucket}")
    private String bucket;
    /**
     * 腾讯OSS应用参数存储桶名称
     **/
    @Value(value = "${oss.tencent.bucketName}")
    private String bucketName;

    /**
     * @Author 小帅丶
     * @Description 删除云存储图片
     * @Date  2020/9/4 16:13
     * @param path 路径
     * @param key 图片名称
     * @return void
     **/
    public void del(String path,String key){
        // 1 初始化用户身份信息(secretId, secretKey)
        COSCredentials cred = new BasicCOSCredentials(accessKey, secretKey);
        // 2 设置bucket的区域 自己的哦
        ClientConfig clientConfig = new ClientConfig(new Region(bucket));
        // 3 生成cos客户端
        COSClient cosClient = new COSClient(cred, clientConfig);
        cosClient.deleteObject(bucketName, path+"/"+key);
        cosClient.shutdown();
    }
}
