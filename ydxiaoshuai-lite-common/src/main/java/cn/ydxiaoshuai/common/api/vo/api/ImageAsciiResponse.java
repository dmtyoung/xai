package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * 
 * @author 小帅丶
 * @date 2018年8月14日
 * <p>Description: 图片转字符页面对象</p>
 */
@Data
public class ImageAsciiResponse extends BaseBean{

	private String image;

	public ImageAsciiResponse success(String msg,String msg_zh,String image) {
		this.msg = msg;
		this.msg_zh = msg_zh;
		this.code = 200;
		this.image = image;
		return this;
	}
	public ImageAsciiResponse fail(String msg,String msg_zh, Integer code) {
		this.msg = msg;
		this.msg_zh = msg_zh;
		this.code = code;
		return this;
	}
	public ImageAsciiResponse error(String msg,String msg_zh) {
		this.msg = msg;
		this.msg_zh = msg_zh;
		this.code = 500;
		return this;
	}
}
