package cn.ydxiaoshuai.common.api.vo.palm;

import lombok.Data;

/**
 * @Description 特殊信息
 * @author 小帅丶
 * @className PalmOrdersSpecialInfoRequestBean
 * @Date 2020/1/3-11:14
 **/
@Data
public class PalmOrdersSpecialInfoRequestBean {

    /**
     * palm : {"isLeft":false,"hand":{"x":[157,425],"y":[28,494]},"palm":{"score":0.9871429800987244,"x":[0,190],"y":[190,466]},"fingers":{"middle":{"score":0.9997792840003967,"x":[74,121],"y":[1,57]},"ring":{"score":0.9911106824874878,"x":[40,83],"y":[26,82]},"index":{"score":0.9991061091423035,"x":[120,165],"y":[29,84]},"little":{"score":0.9992062449455261,"x":[2,46],"y":[93,147]}},"imgUrl":"https://bs-ai.ggwan.com/hand/product/7d3c4b748cbf42a3b4a5bf0ecd941565202001.jpg","lines":{"LL":{"x":[168,161,160,157,154,149,145,142,134,127,119,112,106,102,98,94,91,90,90,92],"y":[252,257,259,261,264,268,272,275,283,292,301,311,321,329,339,351,362,374,387,401]},"WL":{"x":[163,157,154,152,136,129,124,122,118,113,109,105,101,97,93,88,84,79,74,66],"y":[256,259,260,261,270,274,276,278,281,283,286,289,292,294,297,301,304,308,312,319]},"AL":{"x":[125,114,106,97,91,82,76,72,65,62,54,50,46,42,37,32,28,22,17,5],"y":[254,260,264,268,271,275,278,279,282,283,286,287,289,290,292,293,294,296,297,300]}},"handUrl":"https://bs-ai.ggwan.com/hand/product/7d3c4b748cbf42a3b4a5bf0ecd941565202001.jpg"}
     * distribution_info : {"ucode":"","direct":"0"}
     * wechat_info : {"openid":"","nickname":"","gender":"","headimgurl":"","share_openid":"","unionid":"","score":83}
     */

    private String palm;
    private DistributionInfoBean distribution_info;
    private WechatInfoBean wechat_info;

    @Data
    public static class DistributionInfoBean {
        /**
         * ucode :
         * direct : 0
         */

        private String ucode;
        private String direct="0";

    }

    @Data
    public static class WechatInfoBean {
        /**
         * openid :
         * nickname :
         * gender :
         * headimgurl :
         * share_openid :
         * unionid :
         * score : 83
         */

        private String openid;
        private String nickname;
        private String gender;
        private String headimgurl;
        private String share_openid;
        private String unionid;
        private int score;

    }
}
