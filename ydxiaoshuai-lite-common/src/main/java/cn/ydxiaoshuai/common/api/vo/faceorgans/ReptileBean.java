package cn.ydxiaoshuai.common.api.vo.faceorgans;

import lombok.Data;

/**
 *
 * @author 小帅丶
 *
 */
@Data
public class ReptileBean {
    private int status;
    private String msg;
    private Data data;

    @lombok.Data
    public static class Data{
        private String url;//分析后生成的临时图片地址
        private String sign;//签名值
    }
}
