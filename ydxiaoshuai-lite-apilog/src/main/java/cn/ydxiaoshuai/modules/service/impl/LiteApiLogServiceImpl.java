package cn.ydxiaoshuai.modules.service.impl;

import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import cn.ydxiaoshuai.modules.mapper.LiteApiLogMapper;
import cn.ydxiaoshuai.modules.service.ILiteApiLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Service
public class LiteApiLogServiceImpl extends ServiceImpl<LiteApiLogMapper, LiteApiLog> implements ILiteApiLogService {

}
