package cn.ydxiaoshuai.modules.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.entity.LiteConfigCopywrite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 刷新文案
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface LiteConfigCopywriteMapper extends BaseMapper<LiteConfigCopywrite> {

}
